# rentas-demo

= Multi-stage Docker Build for Java Applications

This repository shows how to create a multi-stage Docker build for Java application.

== Demo

. Build: `docker image build -f Dockerfile -t rentasdemo:1 .`
. Run: `docker container run -it -p 8080:8080 rentasdemo:1`
. Access: `curl http://localhost:8080/people/resources/persons`